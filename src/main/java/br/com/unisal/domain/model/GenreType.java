package br.com.unisal.domain.model;

public enum GenreType {

	DRAMA("Drama"),
	HORROR("Horror"),
	COMEDIA("Comedia"),
	ACAO("Ação"),
	AVENTURA("Aventura"),
	DESENHO("Desenho");
	
	
	private final String label;
		
	public String getLabel() {
		return label;
	}

	private GenreType(String label){
		this.label = label;
	}

	
}
