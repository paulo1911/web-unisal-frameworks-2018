package br.com.unisal.domain.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(value=Include.NON_NULL)
public class MovieModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String COLLECTION_NAME = "movies";
	
	
	
	@JsonProperty(value="id")
	private String id;
	
	@JsonProperty(value="title")
	private String title;
	
	@JsonProperty(value="duration")
	private String duration;
	
	@JsonProperty(value="synopsis")
	private String synopsis;
	
	@JsonProperty(value="urlImageBig")
	private String urlImageBig;
	
	@JsonProperty(value="urlImageThumb")
	private String urlImageThumb;
	
	@JsonProperty(value="rating")
	private Long rating;
	
	@JsonProperty(value="year")
	private String year;
	
	@JsonProperty(value="category")
	@JsonFormat(shape= JsonFormat.Shape.STRING)
	private GenreType category;
	
	@JsonProperty(value="maturityRating")
	private Integer maturityRating;
	
	@JsonProperty(value="featured")
	private Boolean featured = Boolean.FALSE;
	
	@JsonProperty(value="included")
	private LocalDateTime included;

	public MovieModel() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public String getUrlImageBig() {
		return urlImageBig;
	}

	public void setUrlImageBig(String urlImageBig) {
		this.urlImageBig = urlImageBig;
	}

	public String getUrlImageThumb() {
		return urlImageThumb;
	}

	public void setUrlImageThumb(String urlImageThumb) {
		this.urlImageThumb = urlImageThumb;
	}

	public Long getRating() {
		return rating;
	}

	public void setRating(Long rating) {
		this.rating = rating;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public GenreType getCategory() {
		return category;
	}

	public void setCategory(GenreType category) {
		this.category = category;
	}

	public Integer getMaturityRating() {
		return maturityRating;
	}

	public void setMaturityRating(Integer maturityRating) {
		this.maturityRating = maturityRating;
	}

	public Boolean isFeatured() {
		return featured;
	}

	public void setFeatured(Boolean featured) {
		this.featured = featured;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((duration == null) ? 0 : duration.hashCode());
		result = prime * result + ((featured == null) ? 0 : featured.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((included == null) ? 0 : included.hashCode());
		result = prime * result + ((maturityRating == null) ? 0 : maturityRating.hashCode());
		result = prime * result + ((rating == null) ? 0 : rating.hashCode());
		result = prime * result + ((synopsis == null) ? 0 : synopsis.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((urlImageBig == null) ? 0 : urlImageBig.hashCode());
		result = prime * result + ((urlImageThumb == null) ? 0 : urlImageThumb.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovieModel other = (MovieModel) obj;
		if (category != other.category)
			return false;
		if (duration == null) {
			if (other.duration != null)
				return false;
		} else if (!duration.equals(other.duration))
			return false;
		if (featured == null) {
			if (other.featured != null)
				return false;
		} else if (!featured.equals(other.featured))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (included == null) {
			if (other.included != null)
				return false;
		} else if (!included.equals(other.included))
			return false;
		if (maturityRating == null) {
			if (other.maturityRating != null)
				return false;
		} else if (!maturityRating.equals(other.maturityRating))
			return false;
		if (rating == null) {
			if (other.rating != null)
				return false;
		} else if (!rating.equals(other.rating))
			return false;
		if (synopsis == null) {
			if (other.synopsis != null)
				return false;
		} else if (!synopsis.equals(other.synopsis))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (urlImageBig == null) {
			if (other.urlImageBig != null)
				return false;
		} else if (!urlImageBig.equals(other.urlImageBig))
			return false;
		if (urlImageThumb == null) {
			if (other.urlImageThumb != null)
				return false;
		} else if (!urlImageThumb.equals(other.urlImageThumb))
			return false;
		if (year == null) {
			if (other.year != null)
				return false;
		} else if (!year.equals(other.year))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MovieModel [id=" + id + ", title=" + title + ", duration=" + duration + ", synopsis=" + synopsis
				+ ", urlImageBig=" + urlImageBig + ", urlImageThumb=" + urlImageThumb + ", rating=" + rating + ", year="
				+ year + ", category=" + category + ", maturityRating=" + maturityRating + ", featured=" + featured
				+ ", included=" + included + "]";
	}
	
}
